<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public $defaultAction = 'login';
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->redirect(array('site/login'));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		if(!Yii::app()->user->isGuest) {
			$this->redirect(array('service/index'));
		}
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(array("service/index"));
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	public function actionChangepassword() {
		if(Yii::app()->user->isGuest  || NULL ===Yii::app()->user->getState('userid')) {
			$this->redirect(array("site/login"));
		}
	    $model = new User;
	    $model = User::model()->findByAttributes(array('id'=>Yii::app()->user->getState('userid')));
	    $model->setScenario('changePwd');
	 
	 
	     if(isset($_POST['User'])){
	 
	        $model->attributes = $_POST['User'];
	        $valid = $model->validate();
	 
	        if($valid){
	 
	          $model->password = $model->new_password;
	 
	          if($model->save())
	             $this->redirect(array('changepassword','msg'=>'successfully changed password'));
	          else
	             $this->redirect(array('changepassword','msg'=>'password not changed'));
	            }
	        }
	 
	    $this->render('changePassword',array('model'=>$model)); 
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}