<div class="row">
	<div class="span8">
		<div class="form">
		 
		<?php $form = $this->beginWidget('BActiveForm', array(
		            'id' => 'chnage-password-form',
		            'enableClientValidation' => true,
		            'htmlOptions' => array('class' => 'well'),
		            'clientOptions' => array(
		                'validateOnSubmit' => true,
		            ),
		     ));
		?>
		 
			<div class="<?php echo $form->fieldClass($model, 'old_password'); ?>">
				<?php echo $form->labelEx($model,'old_password'); ?>
				<div class="input">
					<?php echo $form->passwordField($model,'old_password',array('size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'old_password'); ?>
				</div>
			</div>
		 
			<div class="<?php echo $form->fieldClass($model, 'new_password'); ?>">
				<?php echo $form->labelEx($model,'old_password'); ?>
				<div class="input">
					<?php echo $form->passwordField($model,'new_password',array('size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'new_password'); ?>
				</div>
			</div>
		 
			<div class="<?php echo $form->fieldClass($model, 'new_password'); ?>">
				<?php echo $form->labelEx($model,'repeat_password'); ?>
				<div class="input">
					<?php echo $form->passwordField($model,'repeat_password',array('size'=>60,'maxlength'=>255)); ?>
					<?php echo $form->error($model,'repeat_password'); ?>
				</div>
			</div>
		 
			<div class="actions">
				<?php echo BHtml::submitButton('Change Password'); ?>
			</div>
		  <?php $this->endWidget(); ?>
		</div>
	</div>
</div>