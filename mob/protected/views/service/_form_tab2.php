
	<div class="<?php echo $form->fieldClass($model, 'output_text'); ?>">
		<?php echo $form->labelEx($model,'output_text'); ?>
		<div class="input">
			<?php if($model->isNewRecord) {
				$model->output_text = "<h2>ان لم يفتح تطبيق الرسائل تلقائيا، اضغط على الرابط ادناه</h2><h2>'الرابط'</h2><h2>او ارسل  للرقم 000000</h2>";
			 ?>
				<?php $this->widget('application.extensions.eckeditor.ECKEditor', array(
	            'model'=>$model,
	            'attribute'=>'output_text',
	            'config' => array(
	                'toolbar'=>array(
	                    array( 'Bold', 'Italic', 'Underline', 'Strike' ),
	                    array( 'Source' ,'Link', 'Unlink', 'Anchor','CreatePlaceholder' ) ,
	                ),
	              ),
	            )); ?>
	        <?php } ?>
			<?php echo $form->error($model,'output_text'); ?>
		</div>
	</div>