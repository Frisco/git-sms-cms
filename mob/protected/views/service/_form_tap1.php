
	<div class="<?php echo $form->fieldClass($model, 'name'); ?>">
		<?php echo $form->labelEx($model,'name'); ?>
		<div class="input">
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
	</div>

	<div class="<?php echo $form->fieldClass($model, 'to'); ?>">
		<?php echo $form->labelEx($model,'to'); ?>
		<div class="input">
			<?php echo $form->textField($model,'to',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'to'); ?>
		</div>
	</div>

	<div class="<?php echo $form->fieldClass($model, 'body'); ?>">
		<?php echo $form->labelEx($model,'body'); ?>
		<div class="input">
			<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'body'); ?>
		</div>
	</div>
