<?php
$this->pageCaption='Manage Services';
$this->pageTitle=Yii::app()->name . ' - ' . $this->pageCaption;
$this->pageDescription='Administer all services';
$this->breadcrumbs=array(
	'Services'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Services', 'url'=>array('index')),
	array('label'=>'Create Service', 'url'=>array('create')),
);

?>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'service-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'cssFile'=>Yii::app()->getAssetManager()->publish(Yii::getPathOfAlias('ext.bootstrap-theme.widgets.assets')).'/gridview/styles.css',
	'itemsCssClass'=>'zebra-striped',
	'columns'=>array(
   		 array(
	        'name'=>'name',
	        'type'=>'raw',
	        'value'=>'Chtml::link($data->name,array("view","id"=>$data->id))'
        ),
		'to',
/*    array(
        'name'=>'name',
        'header'=>'Service URL',
        'type'=>'raw',
        'value'=>'Chtml::link("Service Url",array("output/index","id"=>$data->id))',
    ),*/
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}{update}{delete}',
			'buttons' => array(
	           'view' => array( //the name {reply} must be same
	             'label' => 'view', // text label of the button
	               'options'=>array("target"=>"_blank"),
	               'url' => 'CHtml::normalizeUrl(array("output/index&id=".$data->id))', //Your URL According to your wish
	                  'imageUrl' => Yii::app()->baseUrl . '/assets/396a75ae/gridview/view.png', // image URL of the button. If not set or false, a text link is used, The image must be 16X16 pixels
	            ),		
            ),
		),
	),
)); ?>