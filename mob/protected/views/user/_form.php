<div class="form">

<?php $form=$this->beginWidget('BActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php $this->widget('BAlert',array(

		'content'=>'<p>Fields with <span class="required">*</span> are required.</p>'
	)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="<?php echo $form->fieldClass($model, 'username'); ?>">
		<?php echo $form->labelEx($model,'username'); ?>
		<div class="input">
			<?php echo $form->textField($model,'username',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'username'); ?>
		</div>
	</div>
	<?php if($model->isNewRecord) { ?>
		<div class="<?php echo $form->fieldClass($model, 'password'); ?>">
			<?php echo $form->labelEx($model,'password'); ?>
			<div class="input">
				<?php echo $form->passwordField($model,'password',array('size'=>45,'maxlength'=>45)); ?>
				<?php echo $form->error($model,'password'); ?>
			</div>
		</div>
	<?php } ?>

	<div class="<?php echo $form->fieldClass($model, 'type'); ?>">
		<?php echo $form->labelEx($model,'type'); ?>
		<div class="input">
			<?php echo $form->dropDownList($model,'type',array("admin"=>"admin","editor"=>"editor")); ?>
			<?php echo $form->error($model,'type'); ?>
		</div>
	</div>

	<div class="actions">
		<?php echo BHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->